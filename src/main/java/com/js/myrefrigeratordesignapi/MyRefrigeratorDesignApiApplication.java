package com.js.myrefrigeratordesignapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyRefrigeratorDesignApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyRefrigeratorDesignApiApplication.class, args);
    }

}
