package com.js.myrefrigeratordesignapi.model.RefrigeratorIdFood;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class RefrigeratorFoodRequest {
    private String refrigeratorFood;

    private LocalDate dateRefrigeratorFood;
}
