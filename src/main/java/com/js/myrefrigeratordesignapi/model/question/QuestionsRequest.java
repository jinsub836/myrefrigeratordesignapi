package com.js.myrefrigeratordesignapi.model.question;

import com.js.myrefrigeratordesignapi.enmus.QuestionCategory;
import com.js.myrefrigeratordesignapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class QuestionsRequest {

    private QuestionCategory questionCategory;

    private String title;

    private String questionContents;

    private Boolean answerStatus;

    private Boolean isDelete;

    private LocalDate dateDelete;
}
