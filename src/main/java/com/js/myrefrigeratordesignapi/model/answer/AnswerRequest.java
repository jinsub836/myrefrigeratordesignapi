package com.js.myrefrigeratordesignapi.model.answer;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class AnswerRequest {

    private String AnswerTitle;

    private String Answer;

    private Boolean isAnswerDelete;

    private LocalDate dateAnswerDelete;
}
