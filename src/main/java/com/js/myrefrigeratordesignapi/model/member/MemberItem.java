package com.js.myrefrigeratordesignapi.model.member;

import com.js.myrefrigeratordesignapi.enmus.InterLockInfo;
import com.js.myrefrigeratordesignapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class MemberItem {

    private Long Id;

    private String memberName;

    private String memberId;

    private LocalDate dateJoin;

}
