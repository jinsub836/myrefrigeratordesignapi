package com.js.myrefrigeratordesignapi.model.member;

import com.js.myrefrigeratordesignapi.enmus.InterLockInfo;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {
    private String memberName;

    private String memberId;

    private InterLockInfo interlockInfo;

    private String password;

    private String phoneNumber;

    private String mail;

    private LocalDate dateJoin;

    private Boolean isOut;

    private LocalDate dateOut;}
