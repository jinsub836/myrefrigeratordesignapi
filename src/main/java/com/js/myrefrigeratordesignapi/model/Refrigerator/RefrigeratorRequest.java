package com.js.myrefrigeratordesignapi.model.Refrigerator;

import com.js.myrefrigeratordesignapi.entity.RefrigeratorFood;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class RefrigeratorRequest {

   private RefrigeratorFood refrigeratorFood;

   private LocalDate dateRefrigerator;
}
