package com.js.myrefrigeratordesignapi.enmus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum QuestionCategory {
    MEMBER("회원")
    ,POINT("포인트")
    ,CONNECTION("연결에러")
    ,ADVERTISING("광고")
    ,RECIPE("레시피");
    private final String questionName;
}
