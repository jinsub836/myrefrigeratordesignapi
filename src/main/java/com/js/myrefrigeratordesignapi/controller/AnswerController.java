package com.js.myrefrigeratordesignapi.controller;

import com.js.myrefrigeratordesignapi.entity.Questions;
import com.js.myrefrigeratordesignapi.model.answer.AnswerRequest;
import com.js.myrefrigeratordesignapi.service.AnswerService;
import com.js.myrefrigeratordesignapi.service.QuestionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/answer")
public class AnswerController {
    private final AnswerService answerService;
    private final QuestionsService questionsService;

    @PostMapping("/new/{questionsId}")
    public String setQuestion(@PathVariable long questionsId, @RequestBody AnswerRequest request){
        Questions questions = questionsService.getDataIs(questionsId);
        answerService.setAnswer( questions , request);
        return "등록 완료";
    }
}
