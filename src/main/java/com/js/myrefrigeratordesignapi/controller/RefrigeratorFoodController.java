package com.js.myrefrigeratordesignapi.controller;

import com.js.myrefrigeratordesignapi.model.RefrigeratorIdFood.RefrigeratorFoodRequest;
import com.js.myrefrigeratordesignapi.service.RefrigeratorFoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v-1/refrigerator_food")
public class RefrigeratorFoodController {
    private final RefrigeratorFoodService refrigeratorFoodService;


    @PostMapping("/new")
    public String setRefrigeratorFood(RefrigeratorFoodRequest request){
        refrigeratorFoodService.setRefrigeratorFood(request);
        return "음식 등록이 완료되었습니다.";
    }
}
