package com.js.myrefrigeratordesignapi.controller;

import com.js.myrefrigeratordesignapi.repository.RefrigeratorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("v-1/refrigerator")
public class RefrigeratorController {
    private final RefrigeratorRepository refrigeratorRepository;
}
