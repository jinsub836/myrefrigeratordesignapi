package com.js.myrefrigeratordesignapi.controller;

import com.js.myrefrigeratordesignapi.model.member.MemberRequest;
import com.js.myrefrigeratordesignapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/member")
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberRequest request){
        memberService.setMember(request);
        return "회원가입이 완료되었습니다.";
    }
}
