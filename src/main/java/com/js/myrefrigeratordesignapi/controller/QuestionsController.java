package com.js.myrefrigeratordesignapi.controller;

import com.js.myrefrigeratordesignapi.entity.Member;
import com.js.myrefrigeratordesignapi.model.question.QuestionsRequest;
import com.js.myrefrigeratordesignapi.service.MemberService;
import com.js.myrefrigeratordesignapi.service.QuestionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/questions")
@RequiredArgsConstructor
public class QuestionsController {
    private final QuestionsService questionsService;
    private final MemberService memberService;

    @PostMapping("/new/{memberEntityId}")
    public String setQuestion(@PathVariable long memberEntityId, @RequestBody QuestionsRequest request){
        Member member = memberService.getData(memberEntityId);
        questionsService.setQuestion(member, request);
        return "등록 완료";
    }
}
