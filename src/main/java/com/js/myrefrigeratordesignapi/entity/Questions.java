package com.js.myrefrigeratordesignapi.entity;

import com.js.myrefrigeratordesignapi.enmus.QuestionCategory;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Questions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private Member memberEntityId;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private QuestionCategory questionCategory;

    @Column(nullable = false, length = 20)
    private String title;

    @Column(nullable = false , columnDefinition = "TEXT")
    private String questionContents;

    @Column(nullable = false)
    private Boolean answerStatus;

    @Column(nullable = false)
    private LocalDate dateWrite;

    @Column(nullable = false)
    private Boolean isDelete;

    @Column
    private LocalDate dateDelete;
}
