package com.js.myrefrigeratordesignapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class RefrigeratorFood {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    private String refrigeratorFood;

    @Column(nullable = false)
    private LocalDate dateRefrigeratorFood;
}
