package com.js.myrefrigeratordesignapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Refrigerator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private Member memberId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false , name = "refrigeratorId")
    private RefrigeratorFood refrigeratorFood;

    @Column(nullable = false)
    private LocalDate dateRefrigerator;
}

