package com.js.myrefrigeratordesignapi.service;

import com.js.myrefrigeratordesignapi.entity.RefrigeratorFood;
import com.js.myrefrigeratordesignapi.model.RefrigeratorIdFood.RefrigeratorFoodRequest;
import com.js.myrefrigeratordesignapi.repository.RefrigeratorFoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RefrigeratorFoodService {
    private final RefrigeratorFoodRepository refrigeratorFoodRepository;

    public void setRefrigeratorFood(RefrigeratorFoodRequest request){
        RefrigeratorFood addData = new RefrigeratorFood();
        addData.setRefrigeratorFood(request.getRefrigeratorFood());
        addData.setDateRefrigeratorFood(LocalDate.now());

        refrigeratorFoodRepository.save(addData);}
}
