package com.js.myrefrigeratordesignapi.service;


import com.js.myrefrigeratordesignapi.entity.Member;
import com.js.myrefrigeratordesignapi.entity.Refrigerator;
import com.js.myrefrigeratordesignapi.entity.RefrigeratorFood;
import com.js.myrefrigeratordesignapi.model.Refrigerator.RefrigeratorRequest;
import com.js.myrefrigeratordesignapi.repository.RefrigeratorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RefrigeratorService {
    private final RefrigeratorRepository refrigeratorRepository;


    public void setRefrigerator(Member member, RefrigeratorFood refrigeratorFood , RefrigeratorRequest request){
        Refrigerator addData = new Refrigerator();
        addData.setMemberId(member);
        addData.setRefrigeratorFood(refrigeratorFood);
        addData.setDateRefrigerator(LocalDate.now());

        refrigeratorRepository.save(addData);
    }
}
