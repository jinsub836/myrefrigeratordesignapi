package com.js.myrefrigeratordesignapi.service;

import com.js.myrefrigeratordesignapi.entity.Answer;
import com.js.myrefrigeratordesignapi.entity.Questions;
import com.js.myrefrigeratordesignapi.model.answer.AnswerRequest;
import com.js.myrefrigeratordesignapi.repository.AnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class AnswerService {
    private final AnswerRepository answerRepository;

   public void setAnswer(Questions questions ,AnswerRequest request){
       Answer addData = new Answer();
       addData.setQuestionsId(questions);
       addData.setAnswerTitle(request.getAnswerTitle());
       addData.setAnswer(request.getAnswer());
       addData.setDateAnswer(LocalDate.now());
       addData.setIsAnswerDelete(request.getIsAnswerDelete());
       addData.setDateAnswerDelete(request.getDateAnswerDelete());

       answerRepository.save(addData);
   }
}


