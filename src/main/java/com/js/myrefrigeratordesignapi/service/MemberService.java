package com.js.myrefrigeratordesignapi.service;

import com.js.myrefrigeratordesignapi.entity.Member;
import com.js.myrefrigeratordesignapi.model.member.MemberRequest;
import com.js.myrefrigeratordesignapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData (long id){return memberRepository.findById(id).orElseThrow();}

    public void setMember(MemberRequest request){
        Member addData = new Member();

        addData.setMemberName(request.getMemberName());
        addData.setMemberId(request.getMemberId());
        addData.setInterlockInfo(request.getInterlockInfo());
        addData.setPassword(request.getPassword());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMail(request.getMail());
        addData.setDateJoin(LocalDate.now());
        addData.setIsOut(request.getIsOut());
        addData.setDateOut(request.getDateOut());

        memberRepository.save(addData);
    }
}

