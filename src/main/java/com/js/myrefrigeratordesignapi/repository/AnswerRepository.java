package com.js.myrefrigeratordesignapi.repository;

import com.js.myrefrigeratordesignapi.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
