package com.js.myrefrigeratordesignapi.repository;

import com.js.myrefrigeratordesignapi.entity.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Questions , Long> {
}
