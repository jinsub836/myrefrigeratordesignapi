package com.js.myrefrigeratordesignapi.repository;

import com.js.myrefrigeratordesignapi.entity.Refrigerator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefrigeratorRepository extends JpaRepository <Refrigerator , Long > {
}
