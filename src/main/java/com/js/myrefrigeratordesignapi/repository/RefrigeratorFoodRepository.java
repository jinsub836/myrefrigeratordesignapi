package com.js.myrefrigeratordesignapi.repository;

import com.js.myrefrigeratordesignapi.entity.RefrigeratorFood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefrigeratorFoodRepository extends JpaRepository< RefrigeratorFood , Long> {
}
