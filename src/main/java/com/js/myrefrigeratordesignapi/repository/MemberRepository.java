package com.js.myrefrigeratordesignapi.repository;

import com.js.myrefrigeratordesignapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member , Long> {
}
